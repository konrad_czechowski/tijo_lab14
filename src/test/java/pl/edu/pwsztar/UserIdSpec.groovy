package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class UserIdSpec extends Specification {

    @Unroll
    def "should check if #id is valid: #result"() {
        when:
            def pesel = new UserId(id)
        then:
            pesel.isCorrect() == result
        where:
            id              |   result
            '44051401359'   |   true
            '14054401359'   |   true    // switched yy with dd, checksum is still correct
            '44051301358'   |   false
            '02910393811'   |   false
            null            |   false
    }

    @Unroll
    def "should check if #id has correct length"() {
        when:
            def pesel = new UserId(id)
        then:
            pesel.isCorrectSize() == result
        where:
            id              |   result
            '44051401359'   |   true
            '44051301358'   |   true
            '31054401358'   |   true
            ''              |   false
            '635'           |   false
            '987654321234'  |   false
            null            |   false
    }

    @Unroll
    def "should check #id's sex"() {
        when:
            def pesel = new UserId(id)
        then:
            pesel.getSex().orElse(null) == sex
        where:
            id              |   sex
            '44051401359'   |   UserIdChecker.Sex.MAN
            '20220439606'   |   UserIdChecker.Sex.WOMAN
            '04473055202'   |   UserIdChecker.Sex.WOMAN
            '02911393811'   |   UserIdChecker.Sex.MAN
            '14054401359'   |   UserIdChecker.Sex.MAN   // switched yy with dd, still could check person's sex
            '44051301348'   |   null
            'xyxyxyxyx4x'   |   null
            ''              |   null
            '635'           |   null
            '987654321234'  |   null
            null            |   null
    }

    @Unroll
    def "should check #id's birthday date"() {
        when:
            def pesel = new UserId(id)
        then:
            pesel.getDate().orElse(null) == date
        where:
            id              |   date
            '44051401359'   |   '14-05-1944'
            '24841253418'   |   '12-04-1824'
            '10293180609'   |   null
            '15323134811'   |   '31-12-2015'
            '04422973410'   |   '29-02-2104'    // leap year
            '00422969517'   |   null            // not a leap year
            '57702793507'   |   '27-10-2257'
            '10931534816'   |   null
            '44051301348'   |   null
            '44151301348'   |   null
            '02674001348'   |   null
            ''              |   null
            '635'           |   null
            '987654321234'  |   null
            null            |   null
    }
}
