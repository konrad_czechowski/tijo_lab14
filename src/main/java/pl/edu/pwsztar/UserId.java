package pl.edu.pwsztar;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;

final class UserId implements UserIdChecker {

    private final String id;    // NR. PESEL
    private final List<Integer> peselWeightsList = Arrays.asList(9, 7, 3, 1, 9, 7, 3, 1, 9, 7);
    private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");

    public UserId(final String id) {
        this.id = id;
    }

    @Override
    public boolean isCorrectSize() {
        try {
            return id.length() == 11;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public Optional<Sex> getSex() {
        if(!isCorrect()) {
            return Optional.empty();
        }
        try {
            return Optional.of(Integer.parseInt(String.valueOf(id.charAt(9))) % 2 == 0 ? Sex.WOMAN : Sex.MAN);
        } catch(Exception e) {
            return Optional.empty();
        }
    }

    @Override
    public boolean isCorrect() {
        if (!isCorrectSize()) {
            return false;
        }
        try {
            int sum = IntStream.range(0, 10).map(i -> Integer.parseInt(String.valueOf(id.charAt(i))) * peselWeightsList.get(i)).sum();
            return sum % 10 == Integer.parseInt(String.valueOf(id.charAt(10)));
        } catch(Exception e) {
            return false;
        }

    }

    @Override
    public Optional<String> getDate() {
        if(!isCorrect()) {
            return Optional.empty();
        }
        try {
            int day = Integer.parseInt(id.substring(4, 6));
            int month = Integer.parseInt(id.substring(2,4));
            int year = Integer.parseInt(id.substring(0,2));
            if (month >= 1 && month <= 12) {
                year += 1900;
            } else if (month >= 21 && month <= 32) {
                month -= 20;
                year += 2000;
            } else if (month >= 81 && month <= 92) {
                month -= 80;
                year += 1800;
            } else if (month >= 41 && month <= 52) {
                month -= 40;
                year += 2100;
            } else if (month >= 61 && month <= 72) {
                month -= 60;
                year += 2200;
            } else {
                return Optional.empty();
            }
            return Optional.of(LocalDate.of(year, month, day).format(dateTimeFormatter));
        } catch(Exception e) {
            return Optional.empty();
        }
    }
}
